/**
 *
 * В архитектуре нет "черного" и "белого", любое решение - это компромисс. Например, у нас есть система интернет магазин, как на
 * рисунке 03_wb.png. В этой системе есть сервис заказов (где можно заказать товар), сервис принятия платежей (где можно списать
 * деньги за заказ) и складской сервис (узнать остаток товара на складе). Допустим, у нас стал недоступен складской сервис и мы
 * не можем узнать, остался ли у нас вообще товар. Клиент заказывает товар, но мы не знаем, есть ли у нас вообще такой товар из-за
 * недоступности сервиса склада. Как нам поступить?
 *  - принять заказ, списать деньги и ответить клиенту, что все ок, а потом как нибудь разобраться с наличием
 *  - отдать клиенту ошибку, что у сервиса технические неполадки
 * В данном случае, это зависит от предметной области бизнеса. Допустим, мы торгуем одеждой и клиент заказывает у нас джинсы. Не
 * проблема, мы можем принять заказ, списать деньги, и в случае, если на складе не окажется товара, просто вернуть ему деньги и
 * в качестве извенений накинуть каких нибудь бонусов. Но если мы продаем билеты, скажем, на музыкальный фестиваль, который
 * проходит в другой стране - это совсем другая история. Если мы ответим клиенту, что все ок, билеты куплены и спишем деньги, а
 * когда восстановим работу сервиса склада, билетов на самом деле не окажется, мы просто так не отделаемся возвратом денег. К тому
 * моменту клиент уже успеет купить билеты на самолет в ту страну, где проходит фестиваль, заюронировать там отель и взять отпуск
 * на работе. И никакими извинениями мы это не перекроем. Так что тут лучше отдать клиенту ошибку.
 * В общем и целом, любое решение зависит от требований и приоритетов заказчика.
 * Законы архитектуры:
 *  - любое архитектурное решение - это компромисс. Следствие: если архитектор нашел решение, которое не требует компромиссов,
 *      он просто еще не нашел в чем заключается компромисс.
 *  - "почему" важнее, чем "как"
 *
 * */

package m1;

public class Lesson_04 {
}

/**
 *
 * Микросервисы. Вдохновлены Domain Driven Design и концепцией ограниченного контекста - распределние приложения на самодостаточные части m3/06_bounded_context.png.
 * Топология m3/06_mc_topology.png. Микросервисный стиль не накладывает никаких ограничений на формат передачи данных, но нужно придерживаться простой подхода, что
 * канал передачи данных должен отвечать только за передачу данных и больше ничего - никакой доп. логики, трансормации данных и т д. Также желательно, чтобы бд были
 * независимыми для каждого сервиса m3/06_db.pbg. Коммуникация может быть синхронная или асинхронная. БД могут быть людыми у каждого сервиса. Распредлеенные транзакции
 * для обеспечения согласованности данных - это плохо. Лучше пересмотреть границы сервиса или внедрить шаблон Сага.
 * Основные принципы:
 *  - лучше скопировать код, чем выделать в отдельную зависимость
 *  - умные коннекторы, элементарные каналы
 *  - стандартизация подходов к интеграции, а не создание платформы
 *  - децентрализованное упарвление данными
 * Плюсы:
 *  - высокая масштабируемость
 *  - гибкость в изменении функционала
 *  - моудльность
 *  - эластичность
 * Минусы:
 *  - сложность общего решения
 *  - общая стоимость владения
 *  - скорость работы
 *  - сложность создания инфраструктурного ландшафта
 *  - возможность каскадного отключения
 * Никогда не начинать систему сразу как микросервисную, такой стильно должен родиться эволюционно (из монолита), потому что мы не сразу можем определить четкие границы
 * потенциальных сервисов.
 *
 * */

package m3;

public class Lesson_06 {
}
